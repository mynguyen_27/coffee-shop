Feature:
  new bug

  Scenario:
    
    Meta: @abc @def
    
    Narrative: @test
    
    Lifecycle: @bug
    
    Before:
    
    Scope:
    
    After:
    
    Given There has an environment
    When There has an issue
    And It was fixed
    Then I execute test