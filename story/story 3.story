  Scenario:
    <Some interesting scenario steps here>
    
When I add 'commerce' item 'without promotion' with quantity 1

Narrative:
    Given aeSite header set as <aeSite>
    And user type <userType>
    When I add 'commerce' item 'without promotion' with quantity 1
    Then there are no errors and status is success
    When I add shipping address to cart: commerce/data/common/shippingAddress/<shippingAddress>
    Then there are no errors and status is success
    When I add gift card 'Gift_Card_25_USD_03'
    Then there are no errors and status is success
    When I view cart
    Then payment response contains correct recalculated payment amount
    When I add 'commerce' item 'without promotion' with quantity 1
    When I view cart
    Then payment response contains correct recalculated payment amount

